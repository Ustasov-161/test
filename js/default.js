// Маска телефона
$("#phone").mask("+7 (999) 999 - 9999", {
    autoclear: false
});

//Выборка городов
$(".select_city").ready(function () {
    $('select').niceSelect();
});

//Календарь
pickmeup('#datepicker', {
    format: 'd.m.Y',
    default_date: false

});
$('#datepicker').val('');

//Проверка на числа калькулятора по введению
var input = $('.input_form');
input.keyup(function () {
    if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    }
    if (Number(this.value) > 100) {
        this.value = '100';
    }
});


$("#form .button_submit").click(function (event) {
    event.preventDefault();

});


//<<<<<<-----Проверка политики и отправка данных с заявки----->>>>>>//

$('.button_submit').on('click', function (event) {
    var phone = $('input[name="phone"]').val();
    let city = $("#select_city option:selected").text();
    let calendar = $('input[name="calendar"]').val();
    let square = $('input[name="square_ceiling"]').val(),
        lights = $('input[name="lights"]').val(),
        chandelier = $('input[name="chandelier"]').val(),
        tube = $('input[name="tube"]').val(),
        triangle = $('input[name="triangles"]').val(),
        box_facture = $('input[name="glossy"]:checked').val();
    let color = $('input[name="white"]:checked').val();
    let   price_total = $('input[name="price_total"]').val();


    if (phone == '' || phone.slice(-1) == "_") {
        $('input[name="phone"]').addClass('error');
        $('.error_text').fadeIn();
        return false;
    }

    if (!$('#politic').is(':checked')) {
        $('.radio + label').addClass('error_border');
        $('.error_text_checkbox').fadeIn();
        return false;
    }

    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            phone: phone,
            city: city,
            calendar: calendar,
            square: square,
            lights: lights,
            chandelier: chandelier,
            tube: tube,
            triangle: triangle,
            box_facture:box_facture,
            color: color,
            price_total:price_total,
            command: 'leave_app'
        },

        success: function (data) {
        },

        error: function () {
            alert("Что то пошло не так, пользователь не добавлен");
        }
    })
});


$('.btn-secondary').on('click', function (event) {
    location.reload();
});


$(document).ready(function () {
    $('#example').kResponsiveMenu({
        animationSpeed: 'slow', // slow, fast, 200
        resizeWidth: 800, // 'xs', 'sm', 'md', 'lg', 'xl', 480,
        menuIcon: '<i class="fa fa-bars"></i>',
    });
});


$('.add_user_button').on('click', function (event) {
    event.preventDefault();

    var login = $('input[name="add_login"]').val(),
        password = $('input[name="add_password"]').val();


    if (login == '') {
        $('input[name="add_login"]').addClass('error');
        alert('Заполните поле "Логин пользователя"');
        return false;
    } else if (password == '') {
        $('input[name="add_password"]').addClass('error');
        alert('Заполните поле "Пароль пользователя"');
        return false;
    }

    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            login: login, password: password, command: 'add'
        },
        success: function (data) {
            if (data == 'OK') {
               /* $(data).closest(".colour_plus").append();*/
                location.reload(true);
            } else {
                $("#error_add").modal('show');
            }
        },

        error: function (data) {
        }
    })

});


$('.delete_user_button').on('click', function (event) {
    var delete_button = $(this).attr("data-delete");
    var current_element = $(this);

    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            delete_button: delete_button,
            command: 'delete'
        },
        success: function (data) {
            $(current_element).closest("tr").remove();
        },
        error: function () {
            alert("Пользователь не удален");
        },
    })
});


$('.edit_button').on('click', function (event) {
    var edit_button = $(this).attr("data-edit");
    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            edit_button: edit_button,
            command: 'edit'
        },
        success: function (data) {
            var result = jQuery.parseJSON(data);
            var edit_id = $('input[name="edit_id"]').val(result.id),
                edit_login = $('input[name="edit_login"]').val(result.login);
            $("#input_edit").modal('show');
        },
        error: function () {
            alert("Пользователь не удален");
        },
    })
});





// Инпуты и кнопка сохранения нового пользователя
$('.btn_save_class').on('click', function (event) {
    event.preventDefault();

    var save_id = $('input[name="edit_id"]').val(),
        save_login = $('input[name="edit_login"]').val();

    if (save_id == '') {
        $('input[name="edit_id"]').addClass('error');
        alert('Заполните поле "ID"');
        return false;

    } else if (save_login == '') {
        $('input[name="edit_login"]').addClass('error');
        alert('Заполните поле "Логин пользователя"');
        return false;
    }

    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            save_id: save_id,
            save_login: save_login,
            command: 'update'

        },
        success: function (data) {
            if (data == 'OK') {
                $("tr[data-id='" + save_id + "']").find("td:nth-child(2)").html(save_login);
                location.reload(true);
            } else {
                $("#error_add").modal('show');
            }


        },
        error: function () {
            alert("Что то пошло не так");
        }
    })

});

function calculate() {
    var square = $('input[name="square_ceiling"]').val(),
        lights = $('input[name="lights"]').val(),
        chandelier = $('input[name="chandelier"]').val(),
        tube = $('input[name="tube"]').val(),
        triangle = $('input[name="triangles"]').val(),
        box_facture = $('input[name="glossy"]:checked').val();

    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            square: square,
            lights: lights,
            chandelier: chandelier,
            tube: tube,
            triangle: triangle,
            glossy: box_facture,
            command: 'count'
        },

        success: function (data) {
            $('#changeable_price').html(data);
            $('#price_total').val(data);


        },
        error: function () {
            alert("Поробуйте еще разок");
            return false;
        }
    })
}

// Калькулятор для расчета стоимости
$('.js-calc,.js-box,.js-radio').on('keyup', calculate);
$('.box_selected').on('change', calculate);



// Кнопка для удаления цвета
$('.delete_color_button').on('click', function (event) {
    var delete_color = $(this).attr("data-delete");
    var current_element = $(this);

    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            delete_color: delete_color,
            command: 'delete-color'
        },
        success: function (data) {
            var delete_color = jQuery.parseJSON(data);
            $(current_element).closest(".colour_plus").remove();

        },
        error: function () {
            alert("Цвет не удален");
        },
    })
});


// Кнопка "Сохранить" при добавлении нового цвета
$('.btn_save_color').on('click', function (event) {
    event.preventDefault();
    var save_color = $('input[name="color-type"]').val();

    if (save_color == '') {
        $('input[name="color-type"]').addClass('error');
        alert('Заполните какой цветы вы хотите добавить в поле ввода');
        return false;
    }
    ;


    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            save_color: save_color,
            command: 'update-color'

        },
        success: function (data) {
           /* var save_color = jQuery.parseJSON(data);*/
            location.reload(true);


        },
        error: function () {
            alert("Что то пошло не так");
        }
    })

});


$('.js-save-color').on( 'click', function (event) {
    let form = $(".price_form").serialize();


    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: form,

        success: function () {
            location.reload();
        }

    });
    event.preventDefault();
    return false;  //stop the actual form post !important!

});




$('.JsDeleteApplication').on('click', function (event) {
    var delete_application = $(this).attr("data-delete");
    var current_element = $(this);

    $.ajax({
        type: "POST",
        url: '../handler.php',
        data: {
            delete_application: delete_application,
            command: 'delete-application'
        },
        success: function (data) {
            $(current_element).closest("tr").remove();
        },
        error: function () {
            alert("Пользователь не удален");
        },
    })
});