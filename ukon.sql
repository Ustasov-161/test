-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 22 2019 г., 09:31
-- Версия сервера: 5.6.41
-- Версия PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ukon`
--

-- --------------------------------------------------------

--
-- Структура таблицы `request`
--

CREATE TABLE `request` (
  `id` int(10) UNSIGNED NOT NULL,
  `destination_city` text NOT NULL,
  `date_of_birth` text NOT NULL,
  `phone` varchar(30) NOT NULL,
  `text_app` text NOT NULL,
  `date_app` datetime NOT NULL,
  `ip_user` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `request`
--

INSERT INTO `request` (`id`, `destination_city`, `date_of_birth`, `phone`, `text_app`, `date_app`, `ip_user`) VALUES
(247, 'Город не указан', 'Пользователь не указал', '+7 (889) 199 - 1919', '<br><br><br><br><br>Фактура : Матовая<br><br>', '2019-01-17 16:51:08', '127.0.0.1'),
(248, 'Город не указан', 'Пользователь не указал', '+7 (886) 325 - 5442', '<br><br><br><br><br>Фактура : Глянцевая<br><br>', '2019-01-21 13:35:29', '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `price_square_meters` float NOT NULL,
  `price_lamps` float NOT NULL,
  `price_chandelier` float NOT NULL,
  `price_tube` float NOT NULL,
  `price_triangle` float NOT NULL,
  `price_glossy_texture` float NOT NULL,
  `price_matt_texture` float NOT NULL,
  `color_options` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `price_square_meters`, `price_lamps`, `price_chandelier`, `price_tube`, `price_triangle`, `price_glossy_texture`, `price_matt_texture`, `color_options`) VALUES
(1, 2231, 1001, 200, 15, 1501, 15, 18, '{\"1\":\"\\u0411\\u0435\\u043b\\u044b\\u0439\",\"2\":\"\\u0421\\u0435\\u0440\\u044b\\u0439\",\"3\":\"\\u0427\\u0435\\u0440\\u043d\\u044b\\u0439\",\"4\":\"\\u0421\\u0438\\u043d\\u0438\\u0439\"}');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` text NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(164, 'user1', '1'),
(165, 'GEORGE', '1'),
(166, 'guest6', '1'),
(191, 'admin', '$2y$10$M.PUD9uFzV8wz2Dmuk1tneprpnRB3EyYFGfJkpIMAsFh81MagAiuu');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `request`
--
ALTER TABLE `request`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
