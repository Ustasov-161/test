<?php


class Calculator
{

    public static function AllSettings()
    {
        $allSettings = array();
        $conn = DB::getInstance();

            $sql = "SELECT * FROM `settings` LIMIT 1";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $allSettings = $row;

                }
            }

        return $allSettings;
    }



    public static function addColor($json_string)
    {
        $conn = DB::getInstance();
        $json_string = addcslashes($json_string, '\\');
        {
            $sql = "UPDATE `settings` SET `color_options` = '$json_string' WHERE `settings`.`id` = 1;";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $currentColors = $row;
                }
            }
        }
        return $currentColors;

    }


    public static function removeColor($id_delete)
    {
        $settings = self::AllSettings();
        $currentColors = json_decode($settings["color_options"]);

        $conn = DB::getInstance();

        unset($currentColors->$id_delete);

        $new_color_options = addcslashes(json_encode($currentColors), '\\');
        {
            $sql = "UPDATE `settings` SET  `color_options` = '$new_color_options' WHERE `settings` . `id` = 1";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $currentColors = $row;
                }
            }
        }
        return $currentColors;
    }


    public static function updatePrice($ceiling, $chandelier, $triangle,
                                       $glossy_structure, $lights, $matt_texture, $tube)

    {
        $conn = DB::getInstance();
            $sql = "UPDATE `settings` SET `price_square_meters` = '$ceiling', `price_lamps` = '$lights', 
                    `price_chandelier` = '$chandelier', `price_tube` = '$tube', `price_triangle` = '$triangle',
                    `price_glossy_texture` = '$glossy_structure', `price_matt_texture` = '$matt_texture'
                     WHERE `settings`.`id` = 1;";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $array = $row;
                }
            }
        return $array;

    }


    public static function ShowColour() // показывает цвета в index.php в калькуляторе
    {
        $conn = DB::getInstance();

            $sql = "SELECT `color_options` FROM `settings`";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $currentColors  = $row;
                }
            }

        return $currentColors;
    }
}

