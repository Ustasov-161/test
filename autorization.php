<?
include "class/database.php";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $conn = DB::getInstance();
    $login = htmlspecialchars(trim($_POST["login"]));
    $password = htmlspecialchars(trim($_POST["password"]));


    if ($login != "" && $password != "") {
        $query = "SELECT * FROM `users` WHERE `login` = '" . $login . "';";
        $result = $conn->query($query);
        while ($row = $result->fetch_assoc()) {
            $iPassword = $row["password"];
        }
        if (password_verify($password, $iPassword)) {
            setcookie("login", $login, time() + 3600 * 24 * 30, "/");
            setcookie("password", $password, time() + 3600 * 24 * 30, "/");
            header("Location: /admin_panel.php");
        }else {
            $error = "Логин или пароль неправильны";
        }
    } else if (empty($login) && empty($password)){
        $check = "Все поля должны быть заполнены";
    }


}

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>UKON SHOP</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css"
    <link rel="stylesheet" type="text/css" href="css/adaptive.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/autorization.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

</head>

<body>
<div class="col-lg-12 authorization">
    <form action="" method="POST">
        <div class="main">Авторизация</div>
        <div class="enter">Для входа введите логин и пароль</div>
        <div class="col-lg-12 inputs_user_new">
            <label for="login" class="label_login_authorization">Логин:</label>
            <input type="text" id="login" name="login" class="login_input" required placeholder="Логин пользователя">
        </div>

        <div class="col-lg-12 inputs_user_new">
            <label for="password" class="label_password_authorization">Пароль:</label>
            <input type="password" id="password" name="password" class="login_input" required
                   placeholder="Пароль пользователя">
        </div>
        <? if ($error) { ?>
            <div class="mistake"><?= $error; ?></div>
        <? } ?>
        <? if ($check) { ?>
            <div class="check"><?= $check; ?></div>
        <? } ?>
        <div class="submit">
            <button type="submit" class="come_in_authorization">войти</button>
        </div>

    </form>
</div>
</body>
</html>