<?php
include "class/calculator.class.php";
include "class/user.class.php";
include "class/Application.class.php";
include "class/database.php";

// Получаем список всех пользователей
$users = User::getUser();

//Получаем текущие цены калькулятора в инпутах
$price = Calculator::AllSettings();

// Получаем настройки калькулятора
$calculator = Calculator::AllSettings();

// Получаем уже существующие заявку оставленные пользователями
$applications = Application::ShowApplication();


?>


<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>UKON SHOP</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/adaptive.css">
    <link rel="stylesheet" type="text/css" href="/css/menu.css">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

</head>

<body>
<div class="wripersite">
    <header>
        <div class="container head">
            <div class="row header_row">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12  col-xs-12 col-white">
                    <img src="img/logo_header.png" alt="Logo" class="img_logo">
                </div>
                <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6 col-xs-6">
                    <div class="rectangle_one">
                        <div class="geo_icon_head"><i class="fa fa-map-marker" aria-hidden="true"></i>
                            <div class="text_first">г. Ростов-на-Дону, Шаумяна 73 <br>г. Волгодонск, ул. Энтузиастов, 13
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-5 col-md-4 col-sm-6 col-xs-6">
                    <div class="rectangle_two">
                        <div class="icon_phone">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <div class="text_second"><a href="tel:+78632298182">+7 (863) 229-81-82</a>
                                <br> <a href="tel:+78639247979">+7 (8639) 24-79-79</a>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="calc_button">
                        <div class="button_calc">
                            <i class="fa fa-calculator" aria-hidden="true"></i>
                            <div class="calculator_text">Калькулятор онлайн</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <div class="wripernavi">
        <div class="container navi">
            <ul>
                <li><a href="#">о нас</a></li>
                <li><a href="#">каталог</a></li>
                <li><a href="#">услуги</a></li>
                <li><a href="#">цены</a></li>
                <li><a href="#">конструкции</a></li>
                <li><a href="#">акции</a></li>
                <li><a href="#">наши работы</a></li>
                <li><a href="#">контакты</a></li>
            </ul>
        </div>
    </div>
    <header class="bg-light">
        <div class="k-menu-bar container active">

            <button k-toggle-for="#example" class="k-button-toggle navbar-toggler active"><i class="fa fa-bars"></i>
            </button>
        </div>
        <div id="example" class="container k-responsive-menu k-menu-toggle k-active-mobile" style="display: block;">
            <div k-menu-map-to="#logo" class="k-logo navbar-brand" style="display: none;">jQueryScript</div>
            <ul class="nav">
                <li class="nav-item"><a href="#" class="nav-link">О нас</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Каталог</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Услуги</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Цены</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Конструкции</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Акции</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Наши работы</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Контакты</a></li>
            </ul>
        </div>
    </header>

    <div class="container content">
        <div class="row">

            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="users">Пользователи</div>
                <table class="table table-bordered">
                    <tr>
                        <th scope="col" class="left_head">id</th>
                        <th scope="col" class="middle_head">Логин пользователя</th>
                        <th scope="col" class="right_head">Действия</th>
                    </tr>
                    <? foreach ($users as $key => $value) { ?>
                        <tr data-id="<?= $value["id"] ?>">
                            <td class=" <?= (($key % 2) == 0) ? "left_white" : "left_blue"; ?>"><?= $value["id"] ?></td>
                            <td class=" <?= (($key % 2) == 0) ? "middle_white" : "middle_blue"; ?>"><?= $value["login"] ?></td>
                            <td class=" <?= (($key % 2) == 0) ? "right_white" : "right_blue"; ?>">

                                <i class="fa fa-pencil-square-o edit_button" aria-hidden="true"
                                   data-edit="<?= $value["id"] ?>" data-toggle="modal_input"
                                   data-target="#input_edit"> </i>

                                <i class="fa fa-times delete_user_button" aria-hidden="true"
                                   data-delete="<?= $value["id"] ?>"> </i>
                            </td>
                        </tr>
                    <? } ?>
                </table>
            </div>

            <div class="col-lg-12 new_user">Добавление нового пользователя</div>
            <form action="" name="add_user" method="post">
                <div class="col-lg-12 inputs_user_new">
                    <label for="login" class="label_login">Логин:</label>
                    <input type="text" id="login" class="login_input" name="add_login" placeholder="Логин пользователя">
                </div>
                <div class="col-lg-12 inputs_user_new">
                    <label for="password" class="label_password">Пароль:</label>
                    <input type="password" id="password" class="login_input" name="add_password"
                           placeholder="Пароль пользователя">
                </div>
                <button type="submit" class="add_user_button">Добавить пользователя</button>
            </form>

            <form action="" name="price" method="post" class="price_form">
                <input type="hidden" name="update_price" value="true">
                <div class="row calculator_settings_price">
                    <div class="col-lg-12 calculator_custom">Настройки калькулятора</div>
                    <div class="col-xl-4 left_cc">

                        <label for="price_square_m" class="advert_calc_customs">Цена за кв.м. потолка:</label>
                        <input type="text" id="price_square_m" class="input_form_customs" placeholder="0" name="ceiling"
                               value="<?= $price["price_square_meters"] ?>">

                        <label for="price_lamp" class="advert_calc_customs ">Цена за светильник:</label>
                        <input type="text" id="price_lamp" class="input_form_customs" placeholder="0" name="lights""chandelier"
                               value="<?= $price["price_lamps"] ?>">

                        <label for="price_corner" class="advert_calc_customs ">Цена за угол:</label>
                        <input type="text" id="price_corner" class="input_form_customs" placeholder="0" name="triangle"
                               value="<?= $price["price_triangle"] ?>">

                    </div>
                    <div class="col-xl-4 middle_cc">
                        <label for="price_glossy" class="advert_calc_customs">Цена за глянцевую фактуру:</label>
                        <input type="text" id="price_glossy" class="input_form_customs" placeholder="0"
                               name="glossy_structure" value="<?= $price["price_glossy_texture"] ?>">

                        <label for="price_chandelier" class="advert_calc_customs">Цена за люстру:</label>
                        <input type="text" id="price_chandelier" class="input_form_customs" placeholder="0"
                               name="chandelier"
                               value="<?= $price["price_chandelier"] ?>">
                    </div>

                    <div class="col-xl-4 right_cc">
                        <label for="price_matt" class="advert_calc_customs">Цена за матовую фактуру:</label>
                        <input type="text" id="price_matt" class="input_form_customs" placeholder="0"
                               name="matt_texture"
                               value="<?= $price["price_matt_texture"] ?>">

                        <label for="price_tube" class="advert_calc_customs">Цена за трубу:</label>
                        <input type="text" id="price_tube" class="input_form_customs" placeholder="0" name="tube"
                               value="<?= $price["price_tube"] ?>">
                    </div>

                    <div class="col-lg-12 color">
                        <div class="select_colour">Варианты цветов:</div>

                        <? $all_color = json_decode($calculator['color_options']); ?>
                        <? foreach ($all_color as $key => $value) { ?>

                            <div class="type_colour">

                                <div class="colour_plus">
                                    <?= $value; ?> <i class="fa fa-times delete_color_button" aria-hidden="true"
                                                      data-delete="<?= $key ?>"> </i>
                                </div>
                            </div>
                        <? } ?>


                        <div class="add_new_colour js-new-color" data-toggle="modal"
                             data-target=".js-modal-add-color">Добавить новый цвет
                            <i class="fa fa-plus another" aria-hidden="true" data-add="add-color"> </i>
                        </div>
                        <div class="save_change">
                            <button type="submit" class="button_add_colour js-save-color">Сохранить изменения</button>


                        </div>
                    </div>
                </div>
            </form>


            <div class="application">Заявки</div>


            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col" class="id">id</th>
                    <th scope="col" class="phone">Телефон</th>
                    <th scope="col" class="birthdate">Дата рождения</th>
                    <th scope="col" class="city_delivery">Город доставки</th>
                    <th scope="col" class="text_app">Текст заявки</th>
                    <th scope="col" class="date_app">Дата заявки</th>
                    <th scope="col" class="ip">ip</th>
                    <th scope="col" class="action">Действие</th>
                </tr>
                </thead>

                <? foreach ($applications as $key => $value) { ?>

                    <tbody>
                    <tr>
                        <th scope="row"><?= $value["id"] ?></th>
                        <td><?= $value["phone"] ?></td>
                        <td><?= $value["date_of_birth"] ?></td>
                        <td><?= $value["destination_city"] ?></td>
                        <td><?= $value["text_app"] ?></td>
                        <td><?= $newDate = date("d.m.Y H:i:s", strtotime($value["date_app"])) ?></td>
                        <td><?= $value["ip_user"] ?></td>
                        <td class="red_cross"><i class="fa fa-times JsDeleteApplication" aria-hidden="true"
                                                 data-delete="<?= $value["id"] ?>"> </i></td>
                    </tr>

                    </tbody>
                <? } ?>
            </table>
        </div>
    </div>
</div>
<footer>
    <div class="container footer_part">
        <div class="row footer">
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                <p>Компания</p>
                <ul>
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Цены</a></li>
                    <li><a href="#">Акции</a></li>
                    <li><a href="#">Наши работы</a></li>
                    <li><a href="#">Гарантия</a></li>
                    <li><a href="#">Калькулятор</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                <p>Каталог <br> потолков</p>
                <ul>
                    <li><a href="#">Глянцевые</a></li>
                    <li><a href="#">Матовые</a></li>
                    <li><a href="#">Сатиновые</a></li>
                    <li><a href="#">С фотопечатью</a></li>
                    <li><a href="#">Звездное небо</a></li>
                    <li><a href="#">Художественные</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                <p>Услуги</p>
                <ul>
                    <li><a href="#">Установка натяжных потолков</a></li>
                    <li><a href="#">Двухуровневые потолки</a></li>
                    <li><a href="#">Потолки с подвеской</a></li>
                    <li><a href="#">Ремонт потолков</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                <p>Конструкции</p>
                <ul>
                    <li><a href="#">Одноуровневые</a></li>
                    <li><a href="#">Двухуровневые</a></li>
                    <li><a href="#">Многоуровневые</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                <div class="footer_icon"></div>

                <div class="ukon">юкон</div>
                <div class="ceiling">натяжные потолки</div>
                <div class="adress">
                    г. Ростов-на-Дону, Шаумяна, 73 <br><a href="tel:+7 (863) 229-81-82">+7 (863) 229-81-82</a> <br>
                    г. Волгоград, Шаумяна, 73 <br> <a href="tel:+7 (863) 229-81-82">+7 (863) 229-81-82</a>
                </div>
            </div>
        </div>
        <div class="row col-lg-12 end">
            <div class="create_site">ООО “ИТ-Групп”</div>
            <div class="eurosites"><a href="https://eurosites.ru/">Создание сайта - ЕВРОСАЙТЫ</a></div>
        </div>
    </div>
</footer>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Спасибо за заявку!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Спасибо за Вашу заявку. Наш менеджер свяжется с Вами в ближайшее время!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
    <div class="menu_mobile">

    </div>
</div>

<div class="modal fade" id="input_edit" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Редактирование пользователя</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="text" name="edit_id" hidden>
                <input type="text" name="edit_login">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn_save_class" data-dismiss="modal">Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
    <div class="menu_mobile">

    </div>
</div>

<div class="modal fade js-modal-add-color" id="add_color_modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Добавление цвета</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="key-color">
                <input type="text" name="color-type">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn_save_color" data-dismiss="modal">Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
    <div class="menu_mobile">

    </div>
</div>


<div class="modal fade" id="error_add" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Уведомление об ошибке</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               Такой пользователь уже существует, попробуйте поменять свой логин на какой ни будь другой
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
    <div class="menu_mobile">

    </div>
</div>

<script src="/js/jquery-3.3.1.js"></script>
<script src="/js/pickmeup.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.maskedinput.js"></script>
<script src="/js/jquery.nice-select.js"></script>
<script src="/js/jquery.avgrund.js"></script>
<script src="/js/jquery.avgrund.min.js"></script>
<script src="/js/menu.js"></script>
<script src="/js/default.js"></script>

</body>

</html>
