<?php

include "class/user.class.php";
include "class/database.php";
include "class/Calculator.class.php";
include "class/Application.class.php";

function clearData($data)
{
    return trim(htmlspecialchars($data));
}

$conn = DB::getInstance(); /*new mysqli('localhost', 'root', '', 'ukon');*/

$calculator = Calculator::AllSettings();

$command = $_POST['command'];


if ($command == 'add') {
    $login = clearData($_POST['login']);
    $password = clearData($_POST['password']);

        $query = "SELECT `login` FROM `users` WHERE `login` = '$login'";
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            echo "Пользователь уже существует";

        } else {
            $cryptPass = password_hash($password, PASSWORD_DEFAULT, ['SALT' => SALT]);
            User::addUser($login, $cryptPass);
            echo "OK";
        }



} else if ($command == 'delete') {
    $delete_button = clearData($_POST['delete_button']);
    if ($delete_button) {
        User::deleteUser($delete_button);
    }


} else if ($command == 'edit') {
    $edit_button = clearData($_POST['edit_button']);

    if ($edit_button) {
        $currentUser = User::editUser($edit_button);
        echo json_encode($currentUser);
    }


} else if ($command == 'update') {
    $login = clearData($_POST['save_login']);
    $id = clearData($_POST['save_id']);

    $conn = DB::getInstance();
    $query = "SELECT `login` FROM `users` WHERE `login` = '$login'";
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
        echo "Пользователь уже существует";
    } else {
        echo "OK";
        User::updateUser($login, $id);
    }


    if (empty($login) && empty($password)) {
        $check = "Все поля должны быть заполнены";
    }


} else if ($command == 'count') {

    $square = clearData($_POST['square']);
    $lights = clearData($_POST['lights']);
    $chandelier = clearData($_POST['chandelier']);
    $tube = clearData($_POST['tube']);
    $triangle = clearData($_POST['triangle']);
    $box_facture = clearData($_POST['glossy']);


    if ($box_facture == "Глянцевая") {
        $box_facture = $calculator["price_glossy_texture"];
    } else if ($box_facture == "Матовая") {
        $box_facture = $calculator["price_matt_texture"];
    }


    $total_price = ($calculator['price_square_meters'] * $square * $box_facture)
        + ($calculator['price_lamps'] * $lights) +
        ($calculator['price_chandelier'] * $chandelier) +
        ($calculator['price_tube'] * $tube) +
        ($calculator['price_triangle'] * $triangle);

    echo $total_price . " рублей";


} else if ($command == 'delete-color') {
    $delete_color = clearData($_POST['delete_color']);
    if ($delete_color) {
        $delete_color = Calculator::removeColor($delete_color);
        echo json_encode($delete_color);
    }


} else if ($command == 'update-color') {
    $save_color = clearData($_POST['save_color']); // Переопределяем переменную полученную из JS
    $settings_sql = Calculator::AllSettings(); // делаем выборку всего из таблицы Settings
    $new_add_color = $settings_sql["color_options"]; // Выборка кодированного JSON из таблицы Settings поле цветов
    $new_add_color = json_decode($new_add_color); // Декодируем JSON
    $array = (array)$new_add_color; // Переводим из Объекта в Массив

// Это функция для нахождения самого большого KEY
    function array_key_last($array)
    {
        $key = NULL;
        if (is_array($array)) {
            end($array);
            $key = key($array);
            $key++;
        }
        return $key;
    }

    $keyMax = (array_key_last($array)); // Вызов функции для нахождения самого большого КЕЙ
    $array[$keyMax] = $save_color; // Переопределение функции выше
    $new_json_string = json_encode($array);
    Calculator::addColor($new_json_string);


} else if ($_POST['update_price'] == 'true') {
    $ceiling = clearData($_POST['ceiling']); //переменная "Цена за кв.м. потолка:"
    $chandelier = clearData($_POST['chandelier']);  // переменная "Цена за светильник:"
    $triangle = clearData($_POST['triangle']); // переменная "Цена за угол:"
    $glossy_structure = clearData($_POST['glossy_structure']); // переменная "Цена за глянцевую фактуру:"
    $lights = clearData($_POST['lights']); // переменная "Цена за люстру:"
    $matt_texture = clearData($_POST['matt_texture']); // переменная "Цена за матовую фактуру:"
    $tube = clearData($_POST['tube']); // переменная "Цена за трубу:"


    $input_price = Calculator::updatePrice($ceiling, $chandelier, $triangle,
        $glossy_structure, $lights, $matt_texture, $tube);


} else if ($command == 'leave_app'/* || 1==1*/) { // Оставить заявку

    $phone = clearData($_POST['phone']);   // Это переменная из JS  - "Телефон клиента"
    $radio = clearData($_POST['radio']);   // Это переменная из JS  - "Проверка личных данных"
    $city = clearData($_POST['city']);   // Это переменная из JS  - "Город" // ниже проверка на пустоту данных

    if (clearData(($_POST['price_total'])) == "") {
        $show_price = "";
    } else {
        $show_price = "Итого : " . clearData(($_POST['price_total']));
    } // Цена взятая из JS

    if ($city == "Выберите город") {
        $city = "Город не указан";
    }

    $calendar = clearData($_POST['calendar']);   // Это переменная из JS  - "Дата рождения"// ниже проверка на пустоту данных
    if ($calendar == "") {
        $calendar = "Пользователь не указал";
    }
    if (clearData($_POST['square']) == "") {
        $square = "";
    } else {
        $square = 'Площадь потолка : ' . clearData($_POST['square']) . " кв. м.";   // Это переменная из JS  - "Цена за квадратный метр"
    }

    if (clearData($_POST['lights']) == "") {
        $lights = "";
    } else {
        $lights = 'Светильников : ' . clearData($_POST['lights']) . " шт";   // Это переменная из JS  - "Количество светильников"
    }

    if (clearData($_POST['chandelier']) == "") {
        $chandelier = "";
    } else {
        $chandelier = 'Люстр : ' . clearData($_POST['chandelier']) . " шт";   // Это переменная из JS  - "Количество люстр"
    }

    if (clearData($_POST['tube']) == "") {
        $tube = "";
    } else {
        $tube = 'Труб : ' . clearData($_POST['tube']) . " шт";   // Это переменная из JS  - "Количество труб"
    }

    if (clearData($_POST['triangle']) == "") {
        $triangle = "";
    } else {
        $triangle = 'Углы : ' . clearData($_POST['triangle']) . " шт";   // Это переменная из JS  - "Количество углов"
    }

    // Тип фактуры = матовая  или глянцевая поверхность
    $type_of_facture = clearData($_POST['box_facture']);
    if ($type_of_facture == "Глянцевая") {
        $type_of_facture = "Фактура : Глянцевая";
    } else {
        $type_of_facture = "Фактура : Матовая";
    }

    // Цвет из JS
    if (clearData($_POST['color']) == "") {
        $color = "";
    } else {
        $color = 'Цвет : ' . clearData($_POST['color']);
    }

    // ip address user
    $ip = $_SERVER['REMOTE_ADDR'];


    // datetime of application
    $date_app = date("Y-m-d H:i:s");


    $text_app = $square . "<br>" . $lights . "<br>" . $chandelier . "<br>" . $tube . "<br>" . $triangle . "<br>" . $type_of_facture . "<br>" . $color . "<br>" . $show_price;
    $application = Application::leaveApplication($phone, $city, $calendar, $text_app, $date_app, $ip);

// отправка на мыло
    $message = $city . "\n" . "Телефон" . $phone . "\n" . $text_app;
    $to = "test@mail.ru";
    $subject = "Заявка";
    mail($to, $subject, $message);


} else if ($command == 'delete-application') { // удаление цвета
    $deleteApplication = clearData($_POST['delete_application']);
    Application::deleteApplication($deleteApplication);
}


exit;

