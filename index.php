<?php
include "class/calculator.class.php";
include "class/database.php";
$SelectColour = Calculator::ShowColour();
?>


<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>UKON SHOP</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/adaptive.css">
    <link rel="stylesheet" type="text/css" href="/css/menu.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

</head>

<body>
<div class="wripersite">
    <header>
        <div class="container head">
            <div class="row header_row">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12  col-xs-12 col-white">
                    <img src="img/logo_header.png" alt="Logo" class="img_logo">
                </div>
                <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6 col-xs-6">
                    <div class="rectangle_one">
                        <div class="geo_icon_head"><i class="fa fa-map-marker" aria-hidden="true"></i>
                            <div class="text_first">г. Ростов-на-Дону, Шаумяна 73 <br>г. Волгодонск, ул. Энтузиастов, 13
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-5 col-md-4 col-sm-6 col-xs-6">
                    <div class="rectangle_two">
                        <div class="icon_phone">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <div class="text_second"><a href="tel:+78632298182">+7 (863) 229-81-82</a>
                                <br> <a href="tel:+78639247979">+7 (8639) 24-79-79</a>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="calc_button">
                        <div class="button_calc">
                            <i class="fa fa-calculator" aria-hidden="true"></i>
                            <div class="calculator_text">Калькулятор онлайн</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <div class="wripernavi">
        <div class="container navi">
            <ul>
                <li><a href="#">о нас</a></li>
                <li><a href="#">каталог</a></li>
                <li><a href="#">услуги</a></li>
                <li><a href="#">цены</a></li>
                <li><a href="#">конструкции</a></li>
                <li><a href="#">акции</a></li>
                <li><a href="#">наши работы</a></li>
                <li><a href="#">контакты</a></li>
            </ul>
        </div>
    </div>
    <header class="bg-light">
        <div class="k-menu-bar container active">

            <button k-toggle-for="#example" class="k-button-toggle navbar-toggler active"><i class="fa fa-bars"></i>
            </button>
        </div>
        <div id="example" class="container k-responsive-menu k-menu-toggle k-active-mobile" style="display: block;">
            <div k-menu-map-to="#logo" class="k-logo navbar-brand" style="display: none;">jQueryScript</div>
            <ul class="nav">
                <li class="nav-item"><a href="#" class="nav-link">О нас</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Каталог</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Услуги</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Цены</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Конструкции</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Акции</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Наши работы</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Контакты</a></li>
            </ul>
        </div>
    </header>

    <div class="container content">
        <div class="row">
            <div class="calculator">
                <div class="right_side" id="right">
                    <div class="up_side">
                        <div class="calc"></div>
                        <div class="advert">Калькулятор расчета стоимости</div>
                    </div>
                    <div class="down_side">
                        <div class="characteristics">
                            <div class="capital_headline">характеристики:</div>
                        </div>
                        <div class="texture">
                            <div class="capital_headline">фактура:</div>
                        </div>
                    </div>
                    <div class="left_side_calc">
                        <div class="square">
                            <div class="small_headline">Площадь потолка:</div>

                            <div class="square_meters">кв.м</div>
                            <div class="input_form_wripper"><input type="text" class="input_form js-calc"
                                                                   placeholder="0"
                                                                   name="square_ceiling">
                            </div>
                        </div>
                        <div class="small_headline_quantity">Количество</div>
                        <div class="wrapper_calc">
                            <div class="quantity">
                                <div class="small_headline">светильников:</div>
                                <div class="square_meters">шт</div>
                                <div class="input_form_wripper"><input type="text" class="input_form js-calc"
                                                                       placeholder="0"
                                                                       name="lights">
                                </div>
                            </div>
                            <div class="torcher">
                                <div class="small_headline">люстр:</div>
                                <div class="square_meters">шт</div>
                                <div class="input_form_wripper"><input type="text" class="input_form js-calc"
                                                                       placeholder="0"
                                                                       name="chandelier">
                                </div>
                            </div>
                            <div class="lamps">
                                <div class="small_headline">труб:</div>
                                <div class="square_meters">шт</div>
                                <div class="input_form_wripper"><input type="text" class="input_form js-calc"
                                                                       placeholder="0"
                                                                       name="tube">
                                </div>
                            </div>
                            <div class="tubes">
                                <div class="small_headline">углов:</div>
                                <div class="square_meters">шт</div>
                                <div class="input_form_wripper"><input type="text" class="input_form js-calc"
                                                                       placeholder="0"
                                                                       name="triangles"></div>
                            </div>
                        </div>
                    </div>
                    <div class="right_side_calc">
                        <div class="texture_add">
                            <div class="wripper_checkbox">
                                <span class="small_headline">глянцевая</span>
                                <input type="radio" class="box_selected js-box" name="glossy" id="bright" value="Глянцевая" checked/>
                            </div>
                            <div class="wripper_checkbox_two">
                                <span class="small_headline">матовая</span>
                                <input type="radio" class="box_selected js-box" name="glossy" id="faded" value="Матовая"/>
                            </div>
                        </div>
                        <div class="colour">
                            <div class="capital_headline_new">цвет:</div>

                            <? $typeColour = json_decode($SelectColour['color_options']); ?>
                            <?foreach ($typeColour as $key => $value) { ?>
                            <div class="small_headline_new">
                                <input type="radio" class="radio_add js-radio" name="white" id="coloured<?=$key?>" value="<?=$value?> "/>
                                <label class="small_headline" for="coloured<?=$key?>"> <?=$value?> </label>
                            </div>

                            <? }?>
                        </div>
                    </div>
                </div>
                <div class="left_side" id="left">
                    <div class="total_price">итоговая цена:</div>
                    <div id="changeable_price">0 рублей</div>
                    <div class="line"></div>


                    <form action="call_back" method="post" name="form1" class="call_back_form1">
                        <label class="delivery_city" for="select_city">Город доставки:</label>
                        <select name="list_cities" class="select_city" id="select_city">
                            <option>Выберите город</option>
                            <option>Москва</option>
                            <option>Санкт-Петербург</option>
                            <option>Ростов-на-Дону</option>
                            <option>Воронеж</option>
                            <option>Казань</option>
                        </select>

                        <input type="hidden" id="price_total" name="price_total">

                        <label class="delivery_city" for="datepicker">Дата рождения:</label>
                        <div class="wripper_calendar">
                            <form>
                                <input type="text" name="calendar" class="calendar" id="datepicker" value="">
                            </form>
                        </div>
                        <label class="delivery_city" for="phone">Телефон </label>
                        <div class="phone_block">
                            <input type="text" name="phone" class="calendar" id="phone"
                                   placeholder="+7 (___) ___ - ____">


                            <div class="error_text">Это поле обязательно для заполнения*</div>
                            <div class="error_text_checkbox">Это поле обязательно для заполнения*</div>

                        </div>


                        <button type="submit" class="button_submit" name="submit" data-toggle="modal"
                                data-target="#exampleModalCenter">
                            <span class="text_button">оставить заявку</span>
                        </button>
                        <div class="personal_data">
                            <div class="agree">
                                <input type="checkbox" id="politic"  class="radio" name="political"/>
                                <label for="politic">
                                    <a href="#"> Я согласен на обработку персональных данных*</a>
                                </label>
                            </div>
                        </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container footer_part">
            <div class="row footer_.forow">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                    <p>Компания</p>
                    <ul>
                        <li><a href="#">О нас</a></li>
                        <li><a href="#">Цены</a></li>
                        <li><a href="#">Акции</a></li>
                        <li><a href="#">Наши работы</a></li>
                        <li><a href="#">Гарантия</a></li>
                        <li><a href="#">Калькулятор</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                    <p>Каталог <br> потолков</p>
                    <ul>
                        <li><a href="#">Глянцевые</a></li>
                        <li><a href="#">Матовые</a></li>
                        <li><a href="#">Сатиновые</a></li>
                        <li><a href="#">С фотопечатью</a></li>
                        <li><a href="#">Звездное небо</a></li>
                        <li><a href="#">Художественные</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                    <p>Услуги</p>
                    <ul>
                        <li><a href="#">Установка натяжных потолков</a></li>
                        <li><a href="#">Двухуровневые потолки</a></li>
                        <li><a href="#">Потолки с подвеской</a></li>
                        <li><a href="#">Ремонт потолков</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 footer">
                    <p>Конструкции</p>
                    <ul>
                        <li><a href="#">Одноуровневые</a></li>
                        <li><a href="#">Двухуровневые</a></li>
                        <li><a href="#">Многоуровневые</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                    <div class="footer_icon"></div>

                    <div class="ukon">юкон</div>
                    <div class="ceiling">натяжные потолки</div>
                    <div class="adress">
                        г. Ростов-на-Дону, Шаумяна, 73 <br><a href="tel:+7 (863) 229-81-82">+7 (863) 229-81-82</a> <br>
                        г. Волгоград, Шаумяна, 73 <br> <a href="tel:+7 (863) 229-81-82">+7 (863) 229-81-82</a>
                    </div>
                </div>
            </div>
            <div class="row col-lg-12 end">
                <div class="create_site">ООО “ИТ-Групп”</div>
                <div class="eurosites"><a href="https://eurosites.ru/">Создание сайта - ЕВРОСАЙТЫ</a></div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Спасибо за заявку!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Спасибо за Вашу заявку. Наш менеджер свяжется с Вами в ближайшее время!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
        <div class="menu_mobile">

        </div>
    </div>
    <script src="/js/jquery-3.3.1.js"></script>
    <script src="/js/pickmeup.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.maskedinput.js"></script>
    <script src="/js/jquery.nice-select.js"></script>
    <script src="/js/jquery.avgrund.js"></script>
    <script src="/js/jquery.avgrund.min.js"></script>
    <script src="/js/menu.js"></script>
    <script src="/js/default.js"></script>


</div>
</body>

</html>
